package bittrex

import (
	"fmt"
	"gitlab.com/techviking/signalr/v2"
)

// SubscribeToMarketSummary retrieve a filtered list of market summary deltas by market name.
func (b *bittrex) SubscribeToMarketSummary(market string) error {
	if !b.isSubbedToSummaryDeltas {
		if err := b.socketClient.CallHub(signalr.CallHubPayload{
			Hub:    websocketHub,
			Method: "SubscribeToSummaryDeltas",
		}, &(b.isSubbedToSummaryDeltas)); err != nil {
			return err
		} else if !b.isSubbedToSummaryDeltas {
			return fmt.Errorf("no error returned when subscribing to market summary deltas, " +
				"but response still indicated not subscribed")
		}
	}

	b.summaryDeltaMutex.Lock()
	defer b.summaryDeltaMutex.Unlock()

	if _, ok := b.summaryDeltaSubscriptions[market]; !ok {
		b.summaryDeltaSubscriptions[market] = struct{}{}
	}
	return nil
}

func (b *bittrex) GetMarketSummaryChan() chan Summary {
	return b.summaryDeltaChan
}

// SubscribeToMarketSummaryLite retrieve a filtered list of market summary deltas (lite) by market name.
func (b *bittrex) SubscribeToMarketSummaryLite(market string) error {

	if !b.isSubbedToSummaryLiteDelta {
		if err := b.socketClient.CallHub(signalr.CallHubPayload{
			Hub:    websocketHub,
			Method: "SubscribeToSummaryLiteDeltas",
		}, &(b.isSubbedToSummaryLiteDelta)); err != nil {
			return err
		}
	}

	b.summaryDeltaMutex.Lock()
	defer b.summaryDeltaMutex.Unlock()

	if _, ok := b.summaryLiteDeltaSubscriptions[market]; !ok {
		b.summaryLiteDeltaSubscriptions[market] = struct{}{}
	}

	return nil
}


func (b *bittrex) GetSummaryLiteDeltaChan() chan SummaryLiteDelta {
	return b.summaryLiteDeltaChan
}

// SubscribeToExchange retrieve a filtered list of exchange deltas by market name.
func (b *bittrex) SubscribeToExchange(market string) error {

	if !b.isSubbedToExchangeDeltas {
		var subscribedToExchange bool

		if err := b.socketClient.CallHub(signalr.CallHubPayload{
			Hub:    websocketHub,
			Method: "SubscribeToExchangeDeltas",
			Arguments: []interface{}{
				market,
			},
		}, &subscribedToExchange); err != nil {
			return err
		} else if !subscribedToExchange {
			err = fmt.Errorf("error subscribing to exchange deltas for market %s", market)
			b.errChan <- err
			return err
		}
	}

	b.exchangeDeltaMutex.Lock()
	defer b.exchangeDeltaMutex.Unlock()

	if _, ok := b.exchangeDeltaSubscriptions[market]; !ok {
		b.exchangeDeltaSubscriptions[market] = struct{}{}
	}

	return nil
}

func (b *bittrex) GetExchangeDeltasChan() chan ExchangeDelta {
	return b.exchangeDeltaChan
}

// SubscribeToBalanceChanges allow the consuming application access to the channel used to receive balance changes.
// These are piped in automatically with an authenticated socket.
func (b *bittrex) GetBalanceChangesChan() chan BalanceDelta {
	if b.balanceDeltaChan == nil {
		b.balanceDeltaChan= make(chan BalanceDelta)
	}

	return b.balanceDeltaChan
}

// SubscribeToOrderChanges allow the consuming application access to the channel used to receive order changes.
// These are piped in automatically with an authenticated socket.
func (b *bittrex) GetOrderDeltasChan() chan OrderResponse {
	if b.ordersDeltaChan == nil {
		b.ordersDeltaChan = make(chan OrderResponse)
	}
	return b.ordersDeltaChan
}
