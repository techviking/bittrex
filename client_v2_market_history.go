/*
	As of End of March 2018, Bittrex imposed limitations on their API to 60 calls / second,
	and started internally caching results to these calls such that AT BEST the results are only
	3 minutes old. This means that PubMarketGetLatestTick is pretty much useless.

	If you need candles, I'd recommend using the PubMarketGetTicks call once a minute until you have 'fresh' results,
	then swapping over to the socketSubscription for exchange deltas.  --DM
*/

package bittrex

import (
	"encoding/json"
	"fmt"
)

const (
	//TickIntervalOneMin oneMin = 10 days worth of candles
	TickIntervalOneMin = "oneMin"

	//TickIntervalFiveMin fiveMin = 20 days worth of candles
	TickIntervalFiveMin = "fiveMin"

	//TickIntervalThirtyMin thirtyMin = 40 days worth of candles
	TickIntervalThirtyMin = "thirtyMin"

	//TickIntervalHour hour = 60 days worth of candles
	TickIntervalHour = "hour"

	//TickIntervalDay day = 1385 days (nearly four years)
	TickIntervalDay = "day"
)

// PubMarketGetTicks - /pub/market/getticks
// interval must be one of the TickInterval consts
// WARNING - most recent tick can be older than interval requested due to bittrex caching.
func (b *bittrex) PubMarketGetTicks(market string, interval string) ([]Candle, error) {

	params := map[string]string{
		"marketName":   market,
		"tickInterval": interval,
		"useApi2":      "true",
	}

	parsedResponse, parseErr := b.sendRequest("pub/market/getticks", params)

	if parseErr != nil {
		return nil, parseErr
	}

	var response []Candle

	if err := json.Unmarshal(parsedResponse.Result, &response); err != nil {
		return nil, fmt.Errorf("api error - pub/market/getticks %s", err.Error())
	}

	//clean out responses with nil values.
	var cleanedResponse []Candle
	defaultVal := Candle{}

	for _, curVal := range response {
		if curVal != defaultVal {
			cleanedResponse = append(cleanedResponse, curVal)
		}
	}

	if len(cleanedResponse) == 0 {
		return nil, fmt.Errorf("validate response - all candles had empty values")
	}

	return cleanedResponse, nil
}

// PubMarketGetLatestTick - /pub/market/getticks
// interval must be one of the TickInterval consts
func (b *bittrex) PubMarketGetLatestTick(market string, interval string) (Candle, error) {
	return Candle{}, fmt.Errorf("DEPRECATED - use socket api for exchange deltas to get ticker info")
}

func (b *bittrex) PubMarketGetSummaries() ([]MarketSummaryV2, error) {
	params := map[string]string{
		"useApi2":      "true",
	}

	parsedResponse, parseErr := b.sendRequest("pub/Markets/GetMarketSummaries", params)

	if parseErr != nil {
		return nil, parseErr
	}

	var response []MarketSummaryV2

	if err := json.Unmarshal(parsedResponse.Result, &response); err != nil {
		return nil, fmt.Errorf("api error - pub/markets/getmarketsummaries%s", err.Error())
	}

	return response, nil
}
