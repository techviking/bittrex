package bittrex

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"
)

type queryParams = map[string]string

func (b *bittrex) sendRequest(endpoint string, params queryParams) (*baseResponse, error) {
	fullURI := b.getFullURI(endpoint, params)

	sign := b.sign(fullURI)

	var request *http.Request
	var reqErr error

	if request, reqErr = http.NewRequest("GET", fullURI, nil); reqErr != nil {
		return nil, fmt.Errorf("sendRequest - make request: %s", reqErr.Error())
	}

	request.Header.Add("apisign", sign)

	var (
		resp    *http.Response
		respErr error
	)

	if resp, respErr = b.httpClient.Do(request); respErr != nil {
		return nil, fmt.Errorf("sendRequest - do request: %s", respErr.Error())
	}

	defer func(){
		if err := resp.Body.Close(); err != nil {
			fmt.Printf("Error attempting to close response body (watch out for memory leaks!): %+v", err)
		}
	}()

	var (
		rawBody []byte
		readErr error
	)

	if rawBody, readErr = ioutil.ReadAll(resp.Body); readErr != nil {
		return nil, fmt.Errorf("sendRequest - read response %s", readErr.Error())
	}

	var response baseResponse

	if rawBody == nil || len(rawBody) == 0 {
		response = baseResponse{
			Success: false,
			Message: fmt.Sprintf("Response from API endpoint %s was nil or empty", endpoint),
			Result:  rawBody,
		}
	} else if parseBaseResponseErr := json.Unmarshal(rawBody, &response); parseBaseResponseErr != nil {
		if !strings.Contains(resp.Header.Get("Content-Type"), "application/json") {
			return nil, fmt.Errorf("json response wasn't given: %s", string(rawBody))
		}

		return nil, fmt.Errorf("parseBaseResponseErr for endpoint %s, %+v", endpoint, parseBaseResponseErr.Error())
	}

	if response.Success == false {
		return nil, fmt.Errorf("send Request Endpoint - %s: %s", endpoint, response.Message)
	}

	return &response, nil
}

func (b *bittrex) getFullURI(endpoint string, params queryParams) string {

	apiURI := v1APIURL
	if params["useApi2"] != "" {
		apiURI = v2APIURL
		delete(params, "useApi2")
	}

	fullURI := strings.Join([]string{apiURI, endpoint}, "/")

	u, _ := url.Parse(fullURI)

	query := u.Query()

	nonceTime := time.Now().Unix()

	query.Set("nonce", fmt.Sprintf("%d", nonceTime))
	query.Set("apikey", b.apiKey)

	// try to prevent 304 responses.
	query.Set("_", fmt.Sprintf("%d", nonceTime * 1000))

	for param, value := range params {
		query.Set(param, value)
	}

	u.RawQuery = query.Encode()

	return u.String()
}
