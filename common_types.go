package bittrex

import (
	"gitlab.com/techviking/signalr/v2"
)

/*
bittrex describes decimal values as "string encoded decimals", but the json body does not actually wrap the values in quotes.
Thankfully, the value as originally described is within the bounds of a float64 (18 significant digits, with a precision of 8),
so shouldn't be too much data loss for the consumer of this library to transform the value into a better type.

NOTE:  I'm not using shopspring/decimal here because that library's performance is fucking garbage.  Any significant operation within that library
involves a call to the function 'rescale', which creates (potentially multiple) temporary 'Decimal' values which only live for the purposes of the operation.
*/
type decimal = float64

// ConnectionState int representing current state of the SignalR Client
type ConnectionState = signalr.ConnectionState
