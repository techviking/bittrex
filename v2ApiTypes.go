package bittrex

// MarketV2 - Definition of market from v2 rest endpoints.
type MarketV2 struct {
	BaseCurrency       string    `json:"BaseCurrency"`       // BTC
	BaseCurrencyLong   string    `json:"BaseCurrencyLong"`   // Bitcoin
	Created            Timestamp `json:"TimeStamp"`          // : "2014-07-09T03:21:20.08",
	IsActive           bool      `json:"IsActive"`           // true
	IsRestricted       bool      `json:"IsRestricted"`       // false
	IsSponsored        bool      `json:"IsSponsored"`        // meh
	LogoURL            string    `json:"LogoUrl"`            // ttp://someLogoUrl.com/meh.jpg
	MarketCurrency     string    `json:"MarketCurrency"`     // DYN
	MarketCurrencyLong string    `json:"MarketCurrencyLong"` // Dynamic
	MarketName         string    `json:"MarketName"`         // BTC-DYN
	MinTradeSize       decimal   `json:"MinTradeSize"`       // Don't use this.  Calc based on 50k satoshis instead.
	Notice             *string   `json:"Notice"`             // Null in example
}

// MarketSummary result element as described under /public/getmarketsummaries
type MarketSummaryV2 struct {
	IsVerified bool     `json:"IsVerified"` //always false?
	Market     MarketV2 `json:"Market"`
	Summary    Summary  `json:"Summary"`
}
