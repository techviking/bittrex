package bittrex

func (b *bittrex) pipeEventOrderDelta(order *OrderResponse) {
	if b.ordersDeltaChan != nil {
		b.ordersDeltaChan <- *order
	}
}

func (b *bittrex) pipeBalanceDelta(balance *Balance) {
	if b.balanceDeltaChan != nil {
		b.balanceDeltaChan <- balance.BalanceDelta
	}
}

func (b *bittrex) pipeMarketExchangeDelta(exchangeDelta *ExchangeDelta) {
	b.exchangeDeltaMutex.Lock()
	defer b.exchangeDeltaMutex.Unlock()

	b.exchangeDeltaChan <- *exchangeDelta
}

func (b *bittrex) pipeEventSummaryDelta(summary *SummaryDeltaResponse) {
	b.summaryDeltaMutex.Lock()
	defer b.summaryDeltaMutex.Unlock()

	for _, curDelta := range summary.Deltas {
		b.summaryDeltaChan <- curDelta
	}
}

func (b *bittrex) pipeEventSummaryDeltaLite(summary *SummaryLite) {
	b.summaryLiteDeltaMutex.Lock()
	defer b.summaryLiteDeltaMutex.Unlock()

	for _, curDelta := range summary.Deltas {
		b.summaryLiteDeltaChan <- curDelta
	}
}
